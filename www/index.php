<?php

use App\Taskboard\App;

define('ROOT_DIR', realpath(__DIR__ . DIRECTORY_SEPARATOR . '..'));
define('DB_DIR', ROOT_DIR . DIRECTORY_SEPARATOR . 'db');
define('TEMPLATES_DIR', ROOT_DIR . DIRECTORY_SEPARATOR . 'templates');

define('SESSION_NAME', 'JZ_SSID');

require_once(ROOT_DIR . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php');

try {
    App::create()->run();
} catch (Exception $e) {
    http_response_code(500);
    include '500.php';
}

