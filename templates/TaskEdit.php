<?php include 'part' . DIRECTORY_SEPARATOR . 'header.php'; ?>

            <header class="row align-items-center mb-5">
                <h1 class="col-auto mr-auto"><?=$id->getValue() ? 'Редактирование задачи №' . $id : 'Новая задача'?></h1>
                <div id="headbuttons" class="col-auto">                    
                    <?php if (!$admin): ?>
                        <a href="/admin" class="btn btn-primary " role="button">Войти</a>
                    <?php endif; ?>
                </div>
            </header>
            <main>                
                <form method="POST">
                    <?php $readonly = $admin && $id->getValue() ? 'readonly' : ''; ?>
                    <?php if ($id->getValue()): ?>
                        <input type="hidden" name="id" value="<?=$id?>">
                        <input type="hidden" name="sign" value="<?=$sign?>">                        
                    <?php endif; ?>
                    <div class="form-group row">
                        <label for="username" class="col-sm-2 col-form-label">Имя пользователя</label>
                        <div class="col-sm-10">
                            <input type="text" id="username" name="username" placeholder="Иван Иванов"
                                   class="form-control <?=!$username->isValid() ? 'is-invalid' : ''?>" 
                                   value="<?=$username?>" <?=$readonly?> required>
                            <?php if (!$username->isValid()): ?>
                            <div class="invalid-feedback">
                                <?=$username->getErrors()?>
                            </div>
                            <?php endif; ?>
                        </div>
                        
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-sm-2 col-form-label">E-mail</label>
                        <div class="col-sm-10">
                            <input type="email" id="email" name="email" placeholder="email@example.com"
                                   class="form-control <?=!$email->isValid() ? 'is-invalid' : ''?>"
                                   value="<?=$email?>" <?=$readonly?> required>
                            <?php if (!$email->isValid()): ?>
                            <div class="invalid-feedback">
                                <?=$email->getErrors()?>
                            </div>
                            <?php endif; ?>
                        </div>                        
                    </div>                    
                    <div class="form-group row">
                        <label for="text" class="col-sm-2 col-form-label">Текст задачи</label>
                        <div class="col-sm-10">
                            <textarea id="tasktext" name="text" 
                                      class="form-control <?=!$text->isValid() ? 'is-invalid' : ''?>" 
                                      rows="5" required><?=$text?></textarea>
                            <?php if (!$text->isValid()): ?>
                            <div class="invalid-feedback">
                                <?=$text->getErrors()?>
                            </div>
                            <?php endif; ?>
                        </div>                        
                    </div>
                    <?php if ($admin): ?>
                    <div class="form-check row">                        
                        <div class="col">                            
                            <input type="checkbox" id="solved" name="solved" class="form-check-input" 
                                   <?=$solved->getValue() == 1 ? 'checked' : ''?> value="1">
                            <label for="solved" class="form-check-label">
                                Задача выполнена
                            </label>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="form-group row pt-3">
                        <div class="col">
                            <input type="submit" class="btn btn-primary" value="Сохранить">
                            <input type="reset" class="btn btn-warning" value="Очистить">
                        </div>
                    </div>
                </form>
            </main>
<?php include 'part' . DIRECTORY_SEPARATOR . 'footer.php'; ?>