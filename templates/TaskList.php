<?php include 'part' . DIRECTORY_SEPARATOR . 'header.php'; ?>

            <header class="row align-items-center mb-5">
                <h1 class="col-auto mr-auto">Список задач</h1>
                <div id="headbuttons" class="col-auto">
                    <a href="/edit" class="btn btn-primary " role="button">Новая задача</a>
                    <?php if (!$admin): ?>
                        <a href="/admin" class="btn btn-primary " role="button">Войти</a>
                    <?php endif; ?>
                </div>
            </header>
            <main>                
                <?php include 'part' . DIRECTORY_SEPARATOR . 'listSort.php'; ?>
            
                <ul id="tasklist" class="list-unstyled">
                    <?php foreach($taskList as $task): ?>
                    <li class="mb-3">
                        <div class="card">
                            <div class="card-header">
                                #<?=$task['id']?> от <?=$task['username']?> &lt;<?=$task['email']?>&gt;
                                <span class="badge badge-<?=$task['solved'] ? 'secondary' : 'success'?>"><?=$task['solved'] ? 'Выполнена' : 'Новая'?></span>
                            </div>
                            <div class="card-body">
                            <p class="card-text text-wrap">
                                <?= nl2br($task['text'])?>
                            </p>
                            <?php if ($admin): ?>
                                <a href="/edit?id=<?=$task['id']?>" class="btn btn-primary">Редактировать</a>
                            <?php endif; ?>                            
                            </div>
                        </div>
                    </li>
                    <?php endforeach; ?>                    
                </ul>
            
                <?php include 'part' . DIRECTORY_SEPARATOR . 'listPaging.php'; ?>
            </main>
<?php include 'part' . DIRECTORY_SEPARATOR . 'footer.php'; ?>
