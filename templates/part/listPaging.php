<?php 
$url = "/?";
$url .= $sort != 'id' ? "sort=$sort&" : '';
$url .= $sort != 'id' ? "dir=$dir&" : '';
$url .= 'page='
?>
<nav aria-label="Page navigation">
    <ul class="pagination pagination-lg justify-content-center">        
        <li class="page-item <?=$page == 1 ? 'disabled' : ''?>">
            <a class="page-link" href="<?=$page == 1 ? '#' : ($url . ($page - 1))?>" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
            <span class="sr-only">Пред.</span>
        </a>
        </li>
        <?php for ($i = 1; $i <= $pages; $i++): ?>
            <li class="page-item <?=$page == $i ? 'active' : '' ?>"><a class="page-link " href="<?=$url . $i?>"><?=$i?></a></li>
        <?php endfor; ?>
        <li class="page-item <?=$page == $pages ? 'disabled' : ''?>">
        <a class="page-link" href="<?=$page == $pages ? '#' : ($url . ($page + 1))?>" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
            <span class="sr-only">След.</span>
        </a>
        </li>
    </ul>
</nav>

