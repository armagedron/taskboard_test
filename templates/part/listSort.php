<?php
$sorts = [
    'username' => 'Имени пользователя',
    'email' => 'E-mail',
    'solved' => 'Статусу'
];
?>
<div id="sort" class="mb-3">                
    <label class="pr-2">Сортировать по: </label>
    <div class="btn-group" role="group" aria-label="Basic example">
    <?php foreach ($sorts as $key => $label): ?>
        <a href="/?sort=<?=$key?>&dir=<?=$sort == $key && $dir == 'asc' ? 'desc' : 'asc' ?>" 
           class="btn btn-primary <?=$sort == $key ? 'active' : ''?>" 
           role="button">
            <?=$label?> 
            <?php if ($sort == $key) echo $dir == 'asc' ? '&darr;' : '&uarr;'; ?>
        </a>
    <?php endforeach; ?>
    </div>

</div>