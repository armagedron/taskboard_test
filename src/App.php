<?php
namespace App\Taskboard;

use App\Taskboard\Controller\AuthController;
use App\Taskboard\Controller\TaskEditController;
use App\Taskboard\Controller\TaskListController;
use App\Taskboard\DB\Connection;
use App\Taskboard\Exception\Http404Exception;
use App\Taskboard\Model\Form;
use App\Taskboard\Routing\PathRegexpRule;
use App\Taskboard\Routing\RuleBasedRouter;
use App\Taskboard\View\PlainPHPView;
use App\Taskboard\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use const DB_DIR;
use const SESSION_NAME;
use const TEMPLATES_DIR;

/**
 * App
 * @author Andrey Zamiralov <armagedron@mail.ru>
 */
final class App {
    public static function create(): self {
        return new self();
    }
    
    public function run() {
        try {
            Connection::getInstance()
                ->connect('sqlite:' . DB_DIR . DIRECTORY_SEPARATOR . 'taskboard')
            ;

            $request = Request::createFromGlobals();
            if ($request->cookies->has(SESSION_NAME)) {
                $session = new Session();
                $session->setName(SESSION_NAME);
                $session->start();
                $request->setSession($session);
            }

            $router = RuleBasedRouter::create()
                ->addRule(PathRegexpRule::create("@^/$@", TaskListController::class))
                ->addRule(PathRegexpRule::create("@^/admin.*@", AuthController::class))
                ->addRule(PathRegexpRule::create("@^/edit(/?\?id=\d+)?@", TaskEditController::class))
            ;
            $controller = $router->resolve($request)->handle($request);
            $model = $controller->getModel();
            $view = $controller->getView();
            if (!$view instanceof View) {
                $view = PlainPHPView::create()
                    ->setTemplatesDir(TEMPLATES_DIR)
                    ->setTemplate($view);
            }
            $view->render($model);
        } catch (Http404Exception $e) {
            http_response_code(404);
            PlainPHPView::create()
                ->setTemplatesDir(TEMPLATES_DIR)
                ->setTemplate('404')
                ->render(Form::create())
            ;
        }
    }
}
