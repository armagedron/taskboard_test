<?php
namespace App\Taskboard\DB;

use App\Taskboard\DB\Connection;
use App\Taskboard\Model\Model;
use App\Taskboard\Model\TaskModel;
use Closure;
use InvalidArgumentException;
use PDO;
use PDOException;
use PDOStatement;
use Traversable;

/**
 * TaskPeer
 * @author Andrey Zamiralov <armagedron@mail.ru>
 */
class TaskPeer {    
    private static $inst; 
    private static $columns = [
        'id' => PDO::PARAM_INT,
        'username' => PDO::PARAM_STR,
        'email' => PDO::PARAM_STR,
        'text' => PDO::PARAM_STR,
        'solved' => PDO::PARAM_BOOL
    ];
    
    final public static function create(): self {
        if (self::$inst == null) {
            self::$inst = new static();
        }
        return self::$inst;
    }
    
    final private function __construct() { }
    final private function __clone() { }
    final private function __wakeup() { }


    public function getTable(): string {
            return 'task';
    }
    
    public function getById(int $id) {
        $query = "SELECT " . implode(', ', array_keys(self::$columns)) 
            . " FROM " . $this->getTable()
            . " WHERE id = :id"
        ;        
        $exec = $this->executorForQuery($query);                
        $stmt = $exec(['id' => $id]);
        if ( ($row = $stmt->fetch()) !== false) {
            return new TaskModel($row);
        }
        
        return null;
    }
    
    public function getListGenerator(OrderClause $order = null, LimitClause $limit = null): Closure {
        $query = "SELECT " . implode(', ', array_keys(self::$columns)) 
            . " FROM {$this->getTable()}"
            . ($order ?: '')
            . ($limit ?: '')
        ;
        return $this->queryResultGenerator($query);
    }
    
    public function save(Model $task): bool {
        $fields = array_intersect(
            $task->getPropertyNames(),
            array_keys(self::$columns)
        );
        
        if ($task->offsetExists('id') && (int)$task->offsetGet('id') > 0) {
            $query = "UPDATE {$this->getTable()} SET "
                . implode(', ', 
                    array_map(
                        function($fld) {
                            return "$fld = :$fld";
                        }, 
                        $fields
                    )
                ) . " WHERE id = :id"
            ;
            
        } else {
            $query = "INSERT INTO {$this->getTable()} (" . implode(', ', $fields)
                . ") VALUES (" .       ":" . implode(', :', $fields) . ")"
            ;
        }
        $exec = $this->executorForQuery($query);
        
        $bind_vals = array_intersect_key(
            $task->getValues(),
            self::$columns
                
        );
        $insertStmt = $exec($bind_vals);
        
        return $insertStmt->rowCount() > 0;
    }
    
    public function executorForQuery(string $sqlQuery): Closure {
        $stmt = Connection::getInstance()->getConnection()->prepare($sqlQuery);

        return function ($params = []) use ($stmt): PDOStatement {
            if ( $params instanceof Traversable) {
                $params = iterator_to_array($params);
            }
            if (!is_array($params) ) {
                throw new InvalidArgumentException("Statement executor expects array || Traversable as argument with bind values");
            }

            $executed = $this->bindValues($stmt, $params)->execute();
            if ($executed === false) {
                $errInfo = $stmt->errorInfo();
                throw new PDOException($errInfo[2] ?: 'Statement execution silently returns FALSE!', $errInfo[0]);
            }
            
            return $stmt;
        };
    }

    public function queryResultGenerator(string $sqlQuery, ...$fetchParams): Closure {
        $exec_query = $this->executorForQuery($sqlQuery);

        return function($params = []) use ($exec_query, $fetchParams) {
            $stmt = $exec_query($params);
            $rows_count = 0;
            try {
                    if (count($fetchParams) > 0) {
                            $stmt->setFetchMode(...$fetchParams);
                    }
                    while ( ($row = $stmt->fetch()) !== false) {
                            $rows_count++;
                            yield $row;
                    }
            } finally {
                    $stmt->closeCursor();
            }
            return $rows_count;
        };
    }
    
    public function getTotalCount(): int {
        $exec = $this->executorForQuery("SELECT count(1) FROM {$this->getTable()}");
        $stmt = $exec();
        return (int)$stmt->fetchColumn();
        
    }

    protected function bindValues(PDOStatement $stmt, array $bindVals): PDOStatement {
        foreach ($bindVals as $field => $val) {
            $stmt->bindValue(
                ":$field", 
                $val, 
                $val !== null ? self::$columns[$field] : PDO::PARAM_NULL
            );
        }
        
        return $stmt;
    }
}
