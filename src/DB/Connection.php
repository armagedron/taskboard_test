<?php
namespace App\Taskboard\DB;

use PDO;

/**
 * Connection
 * @author Andrey Zamiralov <armagedron@mail.ru>
 */
class Connection {    
    private static $inst;
    private $connection;
        
    final public static function getInstance(): self {
        if (self::$inst == null) {
            self::$inst = new self();
        }
        return self::$inst;
    }
    
    final private function __construct() { }
    final private function __clone() { }
    final private function __wakeup() { }
    
    public function connect(string $db_dsn, $db_user = null, $db_pass = null) {
        $dbh = new PDO($db_dsn, $db_user, $db_pass);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dbh->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        $this->connection = $dbh;
    }
    
    public function getConnection(): PDO {
        return $this->connection;
    }
}
