<?php
namespace App\Taskboard\DB;

/**
 * LimitClause
 * @author Andrey Zamiralov <armagedron@mail.ru>
 */
class LimitClause {
    
    private $limit = 0;
    private $offset = 0;

    public static function create(): self {
        return new static();
    }
    
    public function setLimit(int $limit): self {
        $this->limit = $limit;
        return $this;
    }
    
    public function getLimit(): int {
        return $this->limit;
    }
    
    public function setOffset(int $offset): self {
        $this->offset = $offset;
        return $this;
    }
    
    public function getOffset(): int {
        return $this->offset;
    }
    
    public function __toString(): string {
        return $this->getLimit() 
            ? " LIMIT {$this->getLimit()}" 
                . ($this->getOffset() ? " OFFSET {$this->getOffset()}" : '')
            : ''
        ;
    }
}
