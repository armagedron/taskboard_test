<?php
namespace App\Taskboard\DB;

use InvalidArgumentException;

/**
 * OrderClause
 * @author Andrey Zamiralov <armagedron@mail.ru>
 */
class OrderClause {
    const DIRECTION_ASC = 'ASC';
    const DIRECTION_DESC = 'DESC';

    private $field = '';
    private $direction = self::DIRECTION_ASC;

    public static function create(): self {
        return new static();
    }
    
    public function setBy(string $field): self {
        if (empty($field)) {
            throw new InvalidArgumentException('Order field is empty');
        }
        $this->field = $field;
        return $this;
    }
    
    public function getBy(): string {
        return $this->field;
    }
    
    public function setDirection(string $dir):self {
        $dir = strtoupper($dir);
        if ($dir !== self::DIRECTION_ASC && $dir !== self::DIRECTION_DESC) {
            throw new InvalidArgumentException('Only ASC or DESC allowed');
        }
        $this->direction = $dir;
        return $this;
    }
    
    public function asc(): self {
        $this->direction = self::DIRECTION_ASC;
        return $this;
    }
    
    public function desc(): self {
        $this->direction = self::DIRECTION_DESC;
        return $this;
    }
    
    public function getDirection(): string {
        return $this->direction;
    }
    
    public function __toString(): string {
        return $this->getBy()
            ? " ORDER BY {$this->getBy()} {$this->getDirection()}"
            : ''
        ;
    }
}
