<?php
namespace App\Taskboard\Model;

use InvalidArgumentException;

/**
 * StringLengthValidator
 * @author Andrey Zamiralov <armagedron@mail.ru>
 */
class StringLengthValidator extends Validator {
    private $min = 0;
    private $max = 0;
    
    public function __construct(int $minLength = 0, int $maxLength = 0, string $errorMessage = '') {
        parent::__construct($errorMessage);
        if ($maxLength > 0 && $minLength > $maxLength) {
            throw new InvalidArgumentException('MIN > MAX length');
        }
        if ($minLength > 0) {
            $this->min = $minLength;
        }
        if ($maxLength > 0) {
            $this->max = $maxLength;
        }        
    }
    
    public function isValid($value): bool {
        $len = mb_strlen($value);
        return 
            $len >= $this->min 
            && ($this->max == 0 || $len <= $this->max)
        ;
    }

}
