<?php
namespace App\Taskboard\Model;

use InvalidArgumentException;

/**
 * Field
 * @author Andrey Zamiralov <armagedron@mail.ru>
 */
class Field {
    private $name = '';
    private $value;
    /**
     * @var Validator[] 
     */
    private $validators = [];
    private $valid = true;
    private $errors = [];
    
    public static function create() {
        return new static();
    }
    
    public function setName(string $name): self {
        if (empty($name)) {
            throw new InvalidArgumentException("Field name is empty");
        }
        $this->name = $name;
        return $this;
    }
    
    public function getName(): string {
        return $this->name;
    }
    
    public function setValidators(Validator ...$validators): self {
        $this->validators = $validators;
        return $this;
    }
    
    public function getValue() {
        return $this->value;
    }
        
    public function __toString() {
        return strval($this->getValue());
    }
    
    public function isValid(): bool {
        return $this->valid;
    }
    
    public function getErrors(string $delimiter = "\n"): string {
        return implode($delimiter, $this->errors);
    }
    
    public function import($value): bool {
    if (!is_string($value) || strlen($value) > 0) {
            $this->value = $value;
        }
        foreach ($this->validators as $v) {
            if (!$v->isValid($value)) {
                $this->valid = false;
                $this->errors[] = $v->getErrorMessage();
            }
        }
        return $this->isValid();
    }
    
}
