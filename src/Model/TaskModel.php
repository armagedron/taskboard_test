<?php
namespace App\Taskboard\Model;

use ArrayObject;

/**
 * TaskModel
 * @author Andrey Zamiralov <armagedron@mail.ru>
 */
class TaskModel extends ArrayObject implements Model {
    
    public static function create(): self {
        return new self();
    }
        
    public function getPropertyNames(): array {
        return [
            'id',
            'username',
            'email',
            'text',
            'solved'
        ];
    }
    
    public function merge(Model $model): Model {
        $values = $model->getValues();
        foreach($this->getPropertyNames() as $propName) {
            if ($model->offsetExists($propName)) {
                $this->offsetSet($propName, $values[$propName]);
            }
        }
        return $this;
    }

    public function getValues(): array {
        return $this->getArrayCopy();
    }

}
