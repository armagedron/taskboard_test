<?php
namespace App\Taskboard\Model;

use ArrayObject;
use InvalidArgumentException;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Form
 * @author Andrey Zamiralov <armagedron@mail.ru>
 */
class Form extends ArrayObject implements Model {

    private $fields = [];

    public static function create() {
        return new static();
    }
    
    public function add(Field $field): self {        
        if ($this->offsetExists($field->getName())) {
            throw new InvalidArgumentException("Form field {$field->getName()} is exists already");
        }
        $this->offsetSet($field->getName(), $field);
        $this->fields[$field->getName()] = $field;
        return $this;
    }
    
    public function getFields(): array {
        return $this->fields;
    }
    
    public function import(ParameterBag $input): self {
        foreach ($this->getFields() as $name => $field) {
            $field->import($input->filter($name, null, FILTER_SANITIZE_STRING));
        }
        return $this;        
    }

    public function getErrors(): array {
        $errors = [];
        foreach ($this->getFields() as $name => $field) {
            $errors[$name] = $field->getErrors();
        }
        return $errors;
    }

    public function getValues(): array {        
        $values = [];
        foreach ($this->getIterator() as $name => $field) {
            if ($field instanceof Field) {
                $values[$name] = $field->getValue();
            } else {
                $values[$name] = $field;
            }
        }
        return $values;
    }

    public function isValid(): bool {
        foreach ($this->getFields() as $field) {
            if (!$field->isValid()) {
                return false;
            }            
        }
        return true;
    }

    public function merge(Model $model): Model {
        $values = $model->getValues();
        foreach ($this->getFields() as $name => $field) {
            if ($model->offsetExists($name)) {
                $field->import($values[$name]);
            }
        }
        return $this; 
    }

    public function getPropertyNames(): array {
        return array_keys($this->getArrayCopy());
    }

}
