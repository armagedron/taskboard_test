<?php
namespace App\Taskboard\Model;

/**
 * Validator
 * @author Andrey Zamiralov <armagedron@mail.ru>
 */
abstract class Validator {
    private $message;
    private $required;
    
    abstract public function isValid($val): bool;
    
    public function __construct(string $errorMessage = '', bool $required = true) {
        $this->message = $errorMessage;
        $this->required = $required;
    }
    
    public function getErrorMessage(): string {
        return $this->message;
    }
    
    public function isRequired(): bool {
        return $this->required;
    }
    
}
