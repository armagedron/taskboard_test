<?php
namespace App\Taskboard\Model;

/**
 * IntegerValidator
 * @author Andrey Zamiralov <armagedron@mail.ru>
 */
class IntegerValidator extends Validator {

    public function isValid($value): bool {
        return 
            ( empty($value) && !$this->isRequired() )
            || filter_var($value, FILTER_VALIDATE_INT) !== false
        ;
    }

}
