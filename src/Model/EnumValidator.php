<?php
namespace App\Taskboard\Model;

use InvalidArgumentException;

/**
 * EnumValidator
 * @author Andrey Zamiralov <armagedron@mail.ru>
 */
class EnumValidator extends Validator {

    private $allowed;

    public function __construct(array $allowed, string $errorMessage = '', bool $required = true) {
        parent::__construct($errorMessage, $required);
        if (count($allowed) == 0) {
            throw new InvalidArgumentException('Allowed values list can not be empty');
        }
        $this->allowed = $allowed;
    }
    public function isValid($value): bool {
        return 
            ( empty($value) && !$this->isRequired() )
            || in_array($value, $this->allowed)
        ;
    }

}
