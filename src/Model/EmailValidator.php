<?php
namespace App\Taskboard\Model;

/**
 * EmailValidator
 * @author Andrey Zamiralov <armagedron@mail.ru>
 */
class EmailValidator extends Validator {
    
    public function isValid($value): bool {
        return
            ( empty($value) && !$this->isRequired() )
            || filter_var($value, FILTER_VALIDATE_EMAIL) !== false
        ;
    }

}
