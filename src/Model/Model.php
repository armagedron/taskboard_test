<?php
namespace App\Taskboard\Model;

use ArrayAccess;

/**
 * Model
 * @author Andrey Zamiralov <armagedron@mail.ru>
 */
interface Model extends ArrayAccess {
    public function merge(Model $model): Model; 
    public function getPropertyNames(): array;
    public function getValues(): array;
    public function getArrayCopy();
}
