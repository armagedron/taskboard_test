<?php
namespace App\Taskboard\Model;

use ArrayObject;

/**
 * TaskListModel
 * @author Andrey Zamiralov <armagedron@mail.ru>
 */
class TaskListModel extends ArrayObject implements Model {
    
    public static function create(): self {
        return new static();
    }
                
    public function merge(Model $model): Model {
        return $this->exchangeArray($model->getArrayCopy());
    }

    public function getValues(): array {
        return $this->getArrayCopy();
    }

    public function getPropertyNames(): array {
        return array_keys($this->getArrayCopy());
    }

}
