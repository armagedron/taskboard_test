<?php
namespace App\Taskboard\Controller;

use App\Taskboard\DB\LimitClause;
use App\Taskboard\DB\OrderClause;
use App\Taskboard\DB\TaskPeer;
use App\Taskboard\Model\EnumValidator;
use App\Taskboard\Model\Field;
use App\Taskboard\Model\Form;
use App\Taskboard\Model\IntegerValidator;
use App\Taskboard\Model\Model;
use App\Taskboard\Model\TaskListModel;
use Symfony\Component\HttpFoundation\Request;

/**
 * TaskListController
 * @author Andrey Zamiralov <armagedron@mail.ru>
 */
class TaskListController extends BaseController {

    public function handle(Request $request): Controller {
        $model = $this->getModel();
        $form = $this->createForm()->import($request->query);
        
        $sort = 'id';
        if ($form['sort']->isValid()) {
            $sort = $form['sort']->getValue();
        }
        $dir = 'desc';
        if ($form['dir']->isValid()) {
            $dir = $form['dir']->getValue();
        }
        $page = 1;
        $limit = 3;
        $totalCount = TaskPeer::create()->getTotalCount();
        $pages = ceil($totalCount / $limit);
        
        if ($form['page']->isValid()) {
            $page = intval($form['page']->getValue());
            if ($page < 1) $page = 1;
        }
        if ($page > $pages) $page = $pages;
        
        $skip = $limit * ($page - 1);
        
        $genList = TaskPeer::create()->getListGenerator(
            OrderClause::create()->setBy($sort)->setDirection($dir),
            LimitClause::create()->setLimit($limit)->setOffset($skip)
        );
        
        $model['sort'] = $sort;
        $model['dir'] = $dir;
        $model['page'] = $page;
        $model['pages'] = $pages;
        $model["taskList"] = $genList();
        $model['admin'] = $request->hasSession() && $request->getSession()->get('admin');
        return $this;
    }

    protected function createModel(): Model {
        return TaskListModel::create();
    }
    
    protected function createForm(): Form {
        return Form::create()
            ->add(
                Field::create()
                    ->setName('sort')
                    ->setValidators(new EnumValidator(['username', 'email', 'solved']))
            )
            ->add(
                Field::create()
                    ->setName('dir')
                    ->setValidators(new EnumValidator(['asc', 'desc']))
            )
            ->add(
                Field::create()
                    ->setName('page')
                    ->setValidators(new IntegerValidator())
            )
        ;
    }

}
