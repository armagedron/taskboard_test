<?php
namespace App\Taskboard\Controller;

use App\Taskboard\Model\Form;
use App\Taskboard\Model\Model;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use const SESSION_NAME;

/**
 * AuthController
 * @author Andrey Zamiralov <armagedron@mail.ru>
 */
class AuthController extends BaseController {
    private static $admins = [
        'admin'
    ];
    public function handle(Request $request): Controller {
        if (
            $request->server->has('PHP_AUTH_USER')
            && in_array($request->server->get('PHP_AUTH_USER'), self::$admins)
        ) {
            if (!$request->hasSession()) {
                $session = new Session();
                $session->setName(SESSION_NAME);
                $session->start();
                $request->setSession($session);
            }
            $request->getSession()->set('admin', true);
        }
        
        $this->redirect('/');
    }
    
    protected function createModel(): Model {
        return new Form();
    }

}
