<?php
namespace App\Taskboard\Controller;

use App\Taskboard\Model\Model;

/**
 * BaseController
 * @author Andrey Zamiralov <armagedron@mail.ru>
 */
abstract class BaseController implements Controller {
    private $model;
    private $view;
    
    abstract protected function createModel(): Model;
    
    public function __construct() {
        $this->model = $this->createModel();
        $this->view = $this->getTemplateName();        
    }
    
    public function getModel(): Model {
        return $this->model;
    }

    public function getView(){
        return $this->view;
    }
    
    public function redirect(string $url) {
        header("Location: $url", true, 302);
        exit;
    }
    
    protected function getTemplateName() {
        $class = get_class($this);
        $classNamePos = (strrpos($class, '\\') ?: -1) + 1;
        return str_replace(
            'Controller',
            '',
            substr($class, $classNamePos)
        );
    }
    
}
