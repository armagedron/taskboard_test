<?php
namespace App\Taskboard\Controller;

use App\Taskboard\Model\Model;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller
 * @author Andrey Zamiralov <armagedron@mail.ru>
 */
interface Controller {
    public function handle(Request $request): self;
    public function getModel(): Model;
    public function getView();
}
