<?php
namespace App\Taskboard\Controller;

use App\Taskboard\DB\TaskPeer;
use App\Taskboard\Exception\Http404Exception;
use App\Taskboard\Model\EmailValidator;
use App\Taskboard\Model\EnumValidator;
use App\Taskboard\Model\Field;
use App\Taskboard\Model\Form;
use App\Taskboard\Model\IntegerValidator;
use App\Taskboard\Model\Model;
use App\Taskboard\Model\StringLengthValidator;
use App\Taskboard\Model\TaskModel;
use Symfony\Component\HttpFoundation\Request;

/**
 * TaskEditController
 * @author Andrey Zamiralov <armagedron@mail.ru>
 */
class TaskEditController extends BaseController {
    
    public static $signSalt = 'gKGi3@uOt:kC';
    
    public function handle(Request $request): Controller {
        $form = $this->getModel();
        $form['admin'] = false;
        $task = TaskModel::create();
        
        if ($request->hasSession() && $request->getSession()->get('admin')) {
            $form['admin'] = true;            
        }
        
        if ($request->query->has('id')) {
            if (!$form['admin']) {
                $this->redirect('/edit');
            }
            
            $idField = Field::create()->setValidators(new IntegerValidator());
            if ($idField->import($request->query->get('id'))) {
                $task = TaskPeer::create()->getById((int)$idField->getValue());
                if ($task instanceof TaskModel) {
                    $taskSign = $this->signTask($task);
                    $form->merge($task);
                    $form->offsetGet('sign')->import($taskSign);
                } else {
                    throw new Http404Exception('Задача не найдена');
                }
            }            
        }
        
        if ($request->isMethod(Request::METHOD_POST)) {
            if (
                $form->import($request->request)->isValid()
                && (
                    !$task->offsetExists('id') 
                    || $taskSign == $this->signTask($form)
                )
            ) {
                $task->merge($form);
                TaskPeer::create()->save($task);
                $this->redirect($request->server->get('HTTP_REFERRER', '/'));
            }            
        }
        
        return $this;
    }
    
    protected function createModel(): Model {
        $form = Form::create()
            ->add(
                Field::create()->setName('id')
                    ->setValidators(new IntegerValidator('Invalid ID', false))
            )
            ->add(
                Field::create()->setName('username')
                    ->setValidators(new StringLengthValidator(2, 64, 'Имя пользователя должно содержать от 2 до 64 символов'))
            )
            ->add(
                Field::create()->setName('email')
                    ->setValidators(new EmailValidator('Укажите корректный email'))
            )
            ->add(
                Field::create()->setName('text')
                    ->setValidators(new StringLengthValidator(1, 0, 'Текст задачи не должен быть пустым'))
            )
            ->add(
                Field::create()->setName('solved')
                    ->setValidators(new EnumValidator([0, 1], '', false))
            )
            ->add(Field::create()->setName('sign'))
        ;
        return $form;
    }
    
    private function signTask(Model $task) {
        $values = $task->getValues();
        return sha1(
            ($task->offsetExists('id') ? $values['id'] : '')
            . ($task->offsetExists('username') ? $values['username'] : '')
            . ($task->offsetExists('email') ? $values['email'] : '')
            . self::$signSalt
        );
    }
}
