<?php
namespace App\Taskboard\View;

use App\Taskboard\Model\Model;
use InvalidArgumentException;

/**
 * PlainPHPView
 * @author Andrey Zamiralov <armagedron@mail.ru>
 */
class PlainPHPView implements View {
    const TEMPLATE_EXT = 'php';

    private $templatesDir = '';
    private $template = '';

    public static function create(): self {
        return new static();
    }
    
    public function setTemplatesDir(string $path): View {
        $path = rtrim($path, DIRECTORY_SEPARATOR);
        if (empty($path) || !is_dir($path) || !is_readable($path)) {
            throw new InvalidArgumentException("Templates dir must be readable directory");
        }
        $this->templatesDir = $path;
        return $this;
    }
    
    public function getTemplatesDir(): string {
        return $this->templatesDir;
    }
            
    public function setTemplate(string $template): View {
        $template = ltrim($template, DIRECTORY_SEPARATOR);
        $tplPath = $this->getTemplatePath($template);
        if (
            empty($template) 
            || !is_file($tplPath) 
            || !is_readable($tplPath)) {
            throw new InvalidArgumentException("Template: $tplPath not found or not readable");
        }
        $this->template = $template;
        return $this;
    }
    
    public function getTemplate(): string {
        return $this->template;
    }
    
    public function render(Model $model) {
        extract($model->getArrayCopy());
        include($this->getTemplatePath());
    }

    protected function getTemplatePath(string $template = null): string {
        if (empty($template)) {
            $template = $this->getTemplate();
        }
        return $this->getTemplatesDir() . DIRECTORY_SEPARATOR 
            . $template . '.' . static::TEMPLATE_EXT
        ;
    }
}
