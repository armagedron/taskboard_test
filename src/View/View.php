<?php
namespace App\Taskboard\View;

use App\Taskboard\Model\Model;

/**
 * View
 * @author Andrey Zamiralov <armagedron@mail.ru>
 */
interface View {
    public function setTemplatesDir(string $path): self;
    public function getTemplatesDir(): string;
    public function setTemplate(string $template): self;
    public function getTemplate(): string;
    public function render(Model $model);
}
