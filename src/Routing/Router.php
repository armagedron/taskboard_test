<?php
namespace App\Taskboard\Routing;

use App\Taskboard\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Router
 * @author Andrey Zamiralov <armagedron@mail.ru>
 */
interface Router {
    public function resolve(Request $request): Controller;
}
