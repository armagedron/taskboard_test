<?php
namespace App\Taskboard\Routing;

use Symfony\Component\HttpFoundation\Request;

/**
 * Rule
 * @author Andrey Zamiralov <armagedron@mail.ru>
 */
interface Rule {
    public function setPattern($pattern): self;
    public function setControllerClass(string $controller): self;
    public function getPattern();
    public function getControllerClass(): string;
    public function match(Request $request): bool;
}
