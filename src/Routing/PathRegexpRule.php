<?php
namespace App\Taskboard\Routing;

use InvalidArgumentException;
use Symfony\Component\HttpFoundation\Request;

/**
 * PathRegexpRule
 * @author Andrey Zamiralov <armagedron@mail.ru>
 */
class PathRegexpRule implements Rule {
    private $controllerClass;
    private $pattern;
    
    public static function create(string $pattern, string $controllerClass): self {
        return new static($pattern, $controllerClass);
    }
    
    public function __construct(string $pattern, string $controllerClass) {
        $this->setPattern($pattern);
        $this->setControllerClass($controllerClass);
    }
    
    public function getControllerClass(): string {
        return $this->controllerClass;
    }
    
    /**
     * @return string
     */
    public function getPattern() {
        return $this->pattern;
    }

    public function match(Request $request): bool {
        return preg_match($this->getPattern(), $request->getPathInfo());
    }

    public function setControllerClass(string $controllerClass): Rule {
        if (empty($controllerClass) || !class_exists($controllerClass)) {
            throw new InvalidArgumentException("Controller class: $controllerClass not found");
        }
        
        $this->controllerClass = $controllerClass;
        return $this;
    }
    
    /**
     * @param string $pattern
     * @return Rule
     * @throws InvalidArgumentException
     */
    public function setPattern($pattern): Rule {
        if (!is_string($pattern) || empty($pattern)) {
            throw new InvalidArgumentException("Path regexp rule must be not empty string");
        }
        $this->pattern = $pattern;
        return $this;
    }

}
