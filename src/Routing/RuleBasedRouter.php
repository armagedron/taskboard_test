<?php
namespace App\Taskboard\Routing;

use App\Taskboard\Controller\Controller;
use App\Taskboard\Exception\Http404Exception;
use Symfony\Component\HttpFoundation\Request;

/**
 * RuleBasedRouter
 * @author Andrey Zamiralov <armagedron@mail.ru>
 */
class RuleBasedRouter implements Router {
    private $rules = [];
    
    /**
     * 
     * @return \static
     */
    public static function create() {
        return new static();
    }
    
    public function addRule(Rule $rule): self {
        $this->rules[] = $rule;
        return $this;
    }
    
    /**
     * @return Rule[]
     */
    public function getRules(): array {
        return $this->rules;
    }
    
    /**
     * @param Request $request
     * @return Controller
     * @throws Http404Exception
     */
    public function resolve(Request $request): Controller {
        foreach ($this->getRules() as $rule) {
            if ($rule->match($request)) {
                $controller = $rule->getControllerClass();
                return new $controller();
            }
        }
        throw new Http404Exception("Can't resolve request");
    }

}
