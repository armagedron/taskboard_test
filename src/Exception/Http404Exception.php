<?php
namespace App\Taskboard\Exception;

use Exception;

/**
 * Http404Exception
 * @author Andrey Zamiralov <armagedron@mail.ru>
 */
class Http404Exception extends Exception {
    
}
